﻿" vimrc         {{{1
"
"
"       快捷键表
"
" <Leader>a=    等号两边对齐
" <Leader>a:    冒号两边对齐
" <Leader>s     关闭buffer并保存
" <Leader>q     关闭buffer不保存
" <Leader>M     标记当前位置
" <Leader>N     清除所有标记
" <Leader><Leader>w     激活EasyMotion
" <F3>          打开NERDTree
" <F4>          打开TagBar
" <F10>         VOoM
" <F11>         全屏
" <C-h>         注释当前行
" <C-n>         multiple cursor选择一个词
" <C-p>         multiple cursor取消选择/回到上一个词
" <C-x>         multiple cursor当前词，选择下一个
" ,c            为当前函数生成注释
" ,g            使用DoxygenToolkit生成注释
" cs"'          surround替换双引号为单引号
" ds"           删除双引号
" ysiw)         添加括号(当前词)
" yss)          添加括号(当前行)

" 设置系统变量  {{{2
if(has("win32") || has("win95") || has("win64") || has("win16")) "判定当前操作系统类型
    let g:iswindows=1
else
    let g:iswindows=0
endif

" 插件管理设置      {{{2
" Pathogen设置      {{{3
runtime bundle/vim-pathogen/autoload/pathogen.vim
call pathogen#infect('addons/{}')

" Vundle设置        {{{3
set nocompatible "不要vim模仿vi模式，建议设置，否则会有很多不兼容的问题
filetype off
if(g:iswindows==1)
    set rtp+=$VIM/vimfiles/bundle/vundle
    call vundle#rc('$VIM/vimfiles/bundle/')
else
    set rtp+=~/.vim/bundle/vundle
    call vundle#rc()
endif

Bundle 'gmarik/vundle'
"Bundle 'tpope/vim-rails'
Bundle 'molokai'
Bundle 'a.vim'
Bundle 'AuthorInfo'
Bundle 'auto_mkdir'
Bundle 'ctrlp.vim'
Bundle 'DoxygenToolkit.vim'
Bundle 'FencView.vim'
Bundle 'othree/html5.vim'
Bundle 'jsbeautify'
Bundle 'teramako/jscomplete-vim'
Bundle 'hallison/vim-markdown'
Bundle 'matchit.zip'
Bundle 'The-NERD-tree'
Bundle 'OmniCppComplete'
Bundle 'Lokaltog/vim-powerline'
Bundle 'fs111/pydoc.vim'
Bundle 'slimv.vim'
Bundle 'surround.vim'
Bundle 'majutsushi/tagbar'
Bundle 'scrooloose/nerdcommenter'
Bundle 'VOoM'
Bundle 'VST'
Bundle 'xml.vim'
" Bundle 'drmingdrmer/xptemplate'
Bundle 'mattn/emmet-vim'
Bundle 'kchmck/vim-coffee-script'
Bundle 'lua.vim'
Bundle 'myhere/vim-nodejs-complete'
Bundle 'digitaltoad/vim-jade'
Bundle 'tpope/vim-pathogen'
Bundle 'Tabular'
Bundle 'TabBar'
Bundle 'jnwhiteh/vim-golang'
Bundle 'OrangeT/vim-csharp'
Bundle 'vim-ruby/vim-ruby'
Bundle 'Valloric/YouCompleteMe'
Bundle 'wlangstroth/vim-racket'
Bundle 'xolox/vim-misc'
"Bundle 'xolox/vim-easytags'
Bundle 'xolox/vim-shell'
Bundle 'terryma/vim-multiple-cursors'
Bundle 'altercation/vim-colors-solarized'
Bundle 'Blackrush/vim-gocode'
Bundle 'juvenn/mustache.vim'
Bundle 'dgryski/vim-godef'
Bundle 'kien/rainbow_parentheses.vim'
Bundle 'Yggdroot/indentLine'
Bundle 'Lokaltog/vim-easymotion'
Bundle 'SirVer/ultisnips'
Bundle 'Raimondi/delimitMate'
Bundle 'terryma/vim-multiple-cursors'
Bundle 'scrooloose/syntastic'
" Bundle 'marijnh/tern_for_vim'
" Bundle 'klen/python-mode'

" 系统设置  {{{2
" 打开文件类型检测  {{{3
filetype plugin indent on

" 设置Leader    {{{3
let mapleader = ','

" 设置缩进      {{{3
if has("autocmd")
    augroup vimrcEx
        au!
        autocmd BufReadPost *
                    \ if line("'\"") > 1 && line("'\"") <= line("$") |
                    \ exe "normal! g`\"" |
                    \ endif
    augroup END
else
    set autoindent " always set autoindenting on 
endif " has("autocmd")

" 使用类Windows操作         {{{3
source $VIMRUNTIME/vimrc_example.vim
source $VIMRUNTIME/mswin.vim
behave mswin

" vim在windows下的编码设置。    {{{3
set fileencodings=utf-8,chinese,latin-1 

" Vim字体设置               {{{3
if (g:iswindows==1)
    "解决consle输出乱码 
    language messages zh_CN.gbk
    set guifont=YaHei_Consolas_Hybrid:h10
else 
    set langmenu=zh_CN.UTF-8
    set guifont=文泉驿等宽微米黑\ 10
endif 


" 解决菜单乱码      {{{3
source $VIMRUNTIME/delmenu.vim 
source $VIMRUNTIME/menu.vim 

" 设置行号，颜色主题等  {{{3
set nu
set ruler
colorscheme molokai
set ambiwidth=double 

" 允许退格键删除和tab操作  {{{3
set smartindent  
set smarttab
set expandtab  
set tabstop=4  
set softtabstop=4  
set shiftwidth=4  
set backspace=2
"set textwidth=79

" 设置ruby缩进为2空格
au FileType ruby setlocal shiftwidth=2 tabstop=2

" 设置补全Tag       {{{3
if(g:iswindows==1)
    autocmd FileType php setlocal dict+=$VIM\dict\php_funclist.txt
    autocmd FileType js set dictionary+=$VIM.'\dict\javascript.dict'
    autocmd FileType js set dictionary+=$VIM.'\dict\nodejs.dict'
    let g:pydiction_location=$VIM.'\dict\complete-dict'
    set tags+=$VIM\dict\mingw_tags
    set tags+=$VIM\dict\qt_tags
    set tags+=$VIM\dict\stl_tags
    set tags+=$VIM\dict\wx_tags
    set tags+=$VIM\dict\sdl_tags
    "let g:ruby_path = ':C:\ruby200\bin'
else
    autocmd FileType php setlocal dict+=~/.vim/dict/php_funclist.txt
    autocmd FileType js set dictionary+='~/.vim/dict/javascript.dict'
    autocmd FileType js set dictionary+='~/.vim/dict/nodejs.dict'
    let g:pydiction_location = '~/.vim/dict/complete-dict'
    set tags+=~/.vim/dict/mingw_tags
    set tags+=~/.vim/dict/qt_tags
    set tags+=~/.vim/dict/stl_tags
    set tags+=~/.vim/dict/wx_tags
    set tags+=~/.vim/dict/sdl_tags
endif 

" 设置补全和文件类型      {{{3
let g:OmniCpp_DefaultNamespaces = [ "std" ]
let g:OmniCpp_DisplayMode = 1 " show all class members
let g:OmniCpp_MayCompleteDot = 1 " autocomplete with .
let g:OmniCpp_MayCompleteArrow = 1 " autocomplete with ->
let g:OmniCpp_MayCompleteScope = 1 " autocomplete with ::
let g:OmniCpp_SelectFirstItem = 2 " select first item (but don't insert)
let g:OmniCpp_NamespaceSearch = 2 " search namespaces in this and included files
let g:OmniCpp_ShowPrototypeInAbbr = 1 " show function prototype (i.e. parameters) in popup window

autocmd BufNewFile,BufRead *.mkd set filetype=mkd
autocmd BufRead,BufNewFile *.erb set filetype=eruby.html
autocmd BufRead,BufNewFile *.js set syntax=jquery
autocmd FileType ruby,eruby set omnifunc=rubycomplete#Complete
autocmd FileType python set omnifunc=pythoncomplete#Complete
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
autocmd FileType javascript set omnifunc=nodejscomplete#CompleteJS
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType css set omnifunc=csscomplete#CompleteCSS
autocmd FileType xml set omnifunc=xmlcomplete#CompleteTags
autocmd FileType cs set omnifunc=syntaxcomplete#Complete 
"autocmd FileType go set omnifunc=gocomplete#Complete
"补全Python
let $PYTHONPATH = "."
let g:jedi#popup_on_dot = 0
"let g:jedi#use_tabs_not_buffers = 0
let g:jedi#show_function_definition = 0
let g:jedi#popup_select_first = 0
"补全Ruby
let g:rubycomplete_buffer_loading = 1
let g:rubycomplete_classes_in_global = 1
let g:rubycomplete_rails = 1
"补全Lua
let g:lua_complete_omni = 1
"cscope show in quickfix
set cscopequickfix=s-,c-,d-,i-,t-,e-
let php_sql_query=1                                                                                        
let php_htmlInStrings=1
" 补全js
let g:jscomplete_use = ['dom', 'moz', 'xpcom', 'es6th']
" 补全node.js
let g:node_usejscomplete = 1

" 设置缩进线            {{{3
set list
set listchars=tab:\|\ 

" 设置VimDiff           {{{3
set diffexpr=MyDiff()
function MyDiff()
  let opt = '-a --binary '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  let eq = ''
  if $VIMRUNTIME =~ ' '
    if &sh =~ '\<cmd'
      let cmd = '""' . $VIMRUNTIME . '\diff"'
      let eq = '"'
    else
      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
    endif
  else
    let cmd = $VIMRUNTIME . '\diff'
  endif
  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
endfunction

"插件设置   {{{2

" 配置Godef {{{3
let g:godef_split=0

" 配置Tagbar Go语言支持    {{{3
au FileType go set fileformat=unix

let g:tagbar_type_go = {
    \ 'ctagstype' : 'go',
    \ 'kinds'     : [
        \ 'p:package',
        \ 'i:imports:1',
        \ 'c:constants',
        \ 'v:variables',
        \ 't:types',
        \ 'n:interfaces',
        \ 'w:fields',
        \ 'e:embedded',
        \ 'm:methods',
        \ 'r:constructor',
        \ 'f:functions'
    \ ],
    \ 'sro' : '.',
    \ 'kind2scope' : {
        \ 't' : 'ctype',
        \ 'n' : 'ntype'
    \ },
    \ 'scope2kind' : {
        \ 'ctype' : 't',
        \ 'ntype' : 'n'
    \ },
    \ 'ctagsbin'  : 'gotags',
    \ 'ctagsargs' : '-sort -silent'
\ }

" 配置TagBar Javascript 支持    {{{3
let g:tagbar_type_javascript = {
    \ 'ctagstype' : 'JavaScript',
    \ 'kinds'     : [
        \ 'o:objects',
        \ 'f:functions',
        \ 'a:arrays',
        \ 's:strings'
    \ ]
\ }

" 配置TagBar    {{{3
nmap <F4> :TagbarToggle<CR>

" 配置Tabular   {{{3
nmap <Leader>a= :Tabularize /=<CR>
vmap <Leader>a= :Tabularize /=<CR>
nmap <Leader>a: :Tabularize /:\zs<CR>
vmap <Leader>a: :Tabularize /:\zs<CR>

inoremap <silent> <Bar>   <Bar><Esc>:call <SID>align()<CR>a
 
function! s:align()
  let p = '^\s*|\s.*\s|\s*$'
  if exists(':Tabularize') && getline('.') =~# '^\s*|' && (getline(line('.')-1) =~# p || getline(line('.')+1) =~# p)
    let column = strlen(substitute(getline('.')[0:col('.')],'[^|]','','g'))
    let position = strlen(matchstr(getline('.')[0:col('.')],'.*|\s*\zs.*'))
    Tabularize/|/l1
    normal! 0
    call search(repeat('[^|]*|',column).'\s\{-\}'.repeat('.',position),'ce',line('.'))
  endif
endfunction

" 配置Vundle    {{{3
let $GIT_SSL_NO_VERIFY = 'true'

" 配置YouCompleteMe {{{3
let g:ycm_path_to_python_interpreter = 'C:\Python27\python.exe'
let g:ycm_global_ycm_extra_conf = $VIM . '/vimfiles/config/ycm.py'
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_key_list_select_completion = ['<Down>']
let g:ycm_key_list_previous_completion = ['<Up>']
let g:ycm_confirm_extra_conf = 0
let g:ycm_filetype_whitelist = {
      \ 'cpp' : 1,
      \ 'python' : 1,
      \ 'javascript' : 1,
      \}

" Rainbow Parentheses始终打开
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

" 指定UltiSnips Python版本
let g:UltiSnipsUsePythonVersion = 2
let g:UltiSnipsSnippetsDir = $VIM . '/vimfiles/config/snippets'

" Voom快捷键    {{{3
map <F10> :Voom<CR>

" 关闭buffer但不关闭当前窗口    {{{3
function! CleanClose(tosave)
if (a:tosave == 1)
    w!
endif
let todelbufNr = bufnr("%")
let newbufNr = bufnr("#")
if ((newbufNr != -1) && (newbufNr != todelbufNr) && buflisted(newbufNr))
    exe "b!".newbufNr
else
    bnext
endif
if (bufnr("%") == todelbufNr)
    new
endif
exe "bd!".todelbufNr
endfunction

" 关闭buffer并保存          {{{4
map <leader>s <Esc>:call CleanClose(1)<CR>
" 关闭buffer但不保存        {{{4
map <leader>q <Esc>:call CleanClose(0)<CR>

" mark快捷键        {{{3
nmap <Leader>M <Plug>MarkToggle 
nmap <Leader>N <Plug>MarkAllClear 

" NERDTree  配置    {{{3
nmap <F3> :NERDTree<cr>
let g:NERDTree_title = "[NERDTree]"
function! NERDTree_Start()
    exe 'NERDTree'
endfunction
function! NERDTree_IsValid()
return 1
endfunction

" 对NERD_commenter的设置        {{{3
let NERDShutUp=1
map <c-h> ,c 

" DoxygenToolkit.vim 由注释生成文档，并且能够快速生成函数标准注释 {{{3
map ,g :Dox<cr>
let g:DoxygenToolkit_authorName="Syndim"
let g:DoxygenToolkit_licenseTag="My own license\<enter>"
let g:DoxygenToolkit_undocTag="DOXIGEN_SKIP_BLOCK"
let g:DoxygenToolkit_briefTag_pre = "@brief\t"
let g:DoxygenToolkit_paramTag_pre = "@param\t"
let g:DoxygenToolkit_returnTag = "@return\t"
let g:DoxygenToolkit_briefTag_funcName = "no"
let g:DoxygenToolkit_maxFunctionProtoLines = 30

" jedi-vim  {{{3
if has('python')
py << EOF
import os
import sys
import vim
if 'VIRTUAL_ENV' in os.environ:
    project_base_dir = os.environ['VIRTUAL_ENV']
    sys.path.insert(0, project_base_dir)
    try:
        activate_this = os.path.join(project_base_dir, 'Scripts/activate_this.py')
        execfile(activate_this, dict(__file__=activate_this))
    except:
        activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
        execfile(activate_this, dict(__file__=activate_this))
EOF
endif
